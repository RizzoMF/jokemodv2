﻿using DMT;
using HarmonyLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

public class VehicleWheelSoundPatch_Init
{
    public class Khaine_VehicleWheelSoundPatch_Logger
    {
        public static bool blDisplayLog = true;

        public static void Log(String strMessage)
        {
            if (blDisplayLog)
                UnityEngine.Debug.Log(strMessage);
        }
    }
	
    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("UpdateWheelsCollision")]
    public class Khaine_UpdateWheelsCollision_Patch
    {
        // Loops around the instructions and removes the return condition.
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            Khaine_VehicleWheelSoundPatch_Logger.Log("Patching UpdateWheelsCollision()");

            // Grab all the instructions
            var codes = new List<CodeInstruction>(instructions);
            for (int i = 0; i < codes.Count; i++)
            {			
                if (codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == (float)6500)
                {
                    Khaine_VehicleWheelSoundPatch_Logger.Log("Patching...");
                    codes[i].operand = (float)13000;
					Khaine_VehicleWheelSoundPatch_Logger.Log("Adjusting 6500 to 13000");
					break;
                }
            }
            Khaine_VehicleWheelSoundPatch_Logger.Log("Done With Patching UpdateWheelsCollision()");

            return codes.AsEnumerable();
        }
    }
}
